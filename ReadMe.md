# Overview

Cup supply service for the beverage project.

For testing

```
export CI_REGISTRY_IMAGE=cup-supply
echo ${CI_REGISTRY_IMAGE}
export TAG=`date  +%Y%m%d-%H%M%S-dev`
echo ${TAG}

./gradlew build

docker build -t ${CI_REGISTRY_IMAGE}:${TAG} .

docker run --read-only ${CI_REGISTRY_IMAGE}:${TAG}

```
